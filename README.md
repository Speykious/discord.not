![spinzuka](/spinzuka.gif)

## DO NOT USE THIS

If you want the real `Discord.Net` project, go here:

- Repository: <https://github.com/discord-net/Discord.Net>
- Documentation: <https://discordnet.dev>
