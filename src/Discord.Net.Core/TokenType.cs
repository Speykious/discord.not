using System;

namespace Discord
{
    /// <summary> Specifies the type of token to use with the client. </summary>
    public enum TokenType
    {
        /// <summary>
        ///     Use at your own risk.
        /// </summary>
        User,
        /// <summary>
        ///     An OAuth2 token type.
        /// </summary>
        Bearer,
        /// <summary>
        ///     A bot token type.
        /// </summary>
        Bot,
        /// <summary>
        ///     A webhook token type.
        /// </summary>
        Webhook
    }
}
