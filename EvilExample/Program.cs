﻿using Discord;
using Discord.Rest;

Console.WriteLine("Hello, World! I am here to make evil >:D");

await using var client = new DiscordRestClient();
await client.LoginAsync(TokenType.User, Environment.GetEnvironmentVariable("TOKEN"));

Console.WriteLine("Getting your guilds...");
var guilds = await client.GetGuildSummariesAsync().FlattenAsync();

foreach (var guild in guilds)
{
    Console.WriteLine($"- {guild.Name}");
}
